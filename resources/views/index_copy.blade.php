<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <!--[if IE]>
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <![endif]-->
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <!-- Page title -->
    <title>PETBRO CATTERY</title>
    <!--[if lt IE 9]>
      <script src="js/respond.js"></script>
      <![endif]-->
    <!-- Bootstrap Core CSS -->
    <link href="{{ asset('css/bootstrap.css') }}" rel="stylesheet" type="text/css">
    <!-- Icon fonts -->
    <link href="{{ asset('fonts/font-awesome/css/font-awesome.min.css') }}" rel="stylesheet" type="text/css">
    <link href="{{ asset('fonts/flaticons/flaticon.css') }}" rel="stylesheet" type="text/css">
    <link href="{{ asset('fonts/glyphicons/bootstrap-glyphicons.css') }}" rel="stylesheet" type="text/css">
    <!-- Google fonts -->
    <link href="https://fonts.googleapis.com/css?family=Lato:400,800" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Baloo+Thambi" rel="stylesheet">
    <!-- Style CSS -->
    <link href="{{ asset('css/style.css') }}" rel="stylesheet">
    <!-- Plugins CSS -->
    <link rel="stylesheet" href="{{ asset('css/plugins.css') }}">
    <!-- Color Style CSS -->
    <link href="{{ asset('styles/maincolors.css') }}" rel="stylesheet">
    <!-- Favicons-->
    <link rel="apple-touch-icon" sizes="72x72" href="/apple-icon-72x72.png">
    <link rel="apple-touch-icon" sizes="114x114" href="/apple-icon-114x114.png">
    <link rel="shortcut icon" href="favicon.ico" type="image/x-icon">
</head>

<body id="page-top">
    @extends('preloader')
    @extends('navbar_header')
    <!-- Full Page Image Background Slider -->
    <div class="slider-container">
        <!-- Controls -->
        <div class="slider-control left inactive"></div>
        <div class="slider-control right"></div>
        <ul class="slider-pagi"></ul>
        <!--Slider -->
        <div class="slider">
            <!-- Slide 0 -->
            <div class="slide slide-0 active" style="background-image:url('img/slide0.jpg')">
                <div class="slide__bg"></div>
                <div class="slide__content">
                    <div class="slide__overlay">
                    </div>
                    <!-- slide text-->
                    <div class="slide__text">
                        <h1 class="slide__text-heading">Sahabat Mainecoon</h1>
                        <div class="hidden-mobile">
                            <p class="lead">Hallo Sahabat mainecoon</p>
                            <a href="gallery.html" class="btn btn-default">our gallery</a>
                        </div>
                    </div>
                </div>
            </div>
            <!-- Slide 1 -->
            <div class="slide slide-1" style="background-image:url('img/slide1.jpg')">
                <div class="slide__bg"></div>
                <div class="slide__content">
                    <div class="slide__overlay">
                    </div>
                    <!-- slide text-->
                    <div class="slide__text">
                        <h1 class="slide__text-heading">We Love Pets!</h1>
                        <div class="hidden-mobile">
                            <p class="lead">We offer all the best quality products for your best friend.</p>
                            <a href="blog.html" class="btn btn-default">our Blog</a>
                        </div>
                    </div>
                </div>
            </div>
            <!-- Slide 2 -->
            <div class="slide slide-2" style="background-image:url('img/slide2.jpg')">
                <div class="slide__bg"></div>
                <div class="slide__content">
                    <div class="slide__overlay">
                    </div>
                    <!-- slide text-->
                    <div class="slide__text">
                        <h1 class="slide__text-heading">Amazing Services</h1>
                        <div class="hidden-mobile">
                            <p class="lead">We offer all the best quality products for your best friend.</p>
                            <a href="about.html" class="btn btn-default">About Us</a>
                        </div>
                    </div>
                </div>
            </div>
            <!--/Slide2 -->
        </div>
        <!--/Slider-->
    </div>
    <!--/ Slider ends -->
    <!-- SVG Curve Up -->
    <svg id="curveUp" xmlns="http://www.w3.org/2000/svg" version="1.1" width="100%" height="100" viewBox="0 0 100 100" preserveAspectRatio="none" fill="#fff">
        <path d="M0 100 C 20 0 50 0 100 100 Z" />
    </svg>
    <!-- Section Services-index -->
    <section id="services-index">
        <!-- container -->
        <div class="container">
            <div class="section-heading">
                <h2>Pelayanan Kami</h2>
            </div>
            <!-- /section-heading-->
            <div class="col-md-10 col-md-offset-1 text-center">
                <p>Ada begitu banyak manfaat mengadopsi kucing, yaitu memberi hewan peliharaan kesempatan kedua dalam kehidupan keluarga yang terpenuhi! Jika Anda bertanya-tanya bagaimana cara mengadopsi kucing dan apa yang diharapkan selama proses tersebut, Sahabat Mainecoon siap membantu.</p>
            </div>
            <!-- /col-md-10-->
        </div>
        <!-- /container-->
        <div class="container-fluid bg-pattern margin1" data-bottom-top="background-position: 0px 70%,99% 70%;" data-top-bottom="background-position: 0px -50%,99% -50%;">
            <div class="row">
                <div class="col-md-10 col-md-offset-1">
                    <!-- Services -->
                    <div id="owl-services" class="owl-carousel">
                        <!-- Feature Box 1  -->
                        <div class="col-xs-12">
                            <div class="box_icon">
                                <div class="icon">
                                    <!-- icon -->
                                    <div class="image">
                                        <img src="{{ asset('img/service1.jpg')}}" class="img-responsive" alt="">
                                    </div>
                                    <div class="info">
                                        <h4>Grooming</h4>
                                        <p>Nulla vel metus scelerisque ante sollicitudinlorem ipsuet commodo. Cras purus odio, vestibulum in vulputate a Imperdiet interdum donec eget metus auguen unc vel lorem.</p>
                                        <!-- Button-->

                                    </div>
                                </div>
                            </div>
                            <!-- /box_icon -->
                        </div>
                        <!-- /col-xs-12 ends -->
                        <!-- Feature Box 2 -->
                        <div class="col-xs-12">
                            <div class="box_icon">
                                <div class="icon">
                                    <!-- icon -->
                                    <div class="image">
                                        <img src="{{asset('img/service2.jpg')}}" class="img-responsive" alt="">
                                    </div>
                                    <div class="info">
                                        <h4>Breeding</h4>
                                        <p>Nulla vel metus scelerisque ante sollicitudinlorem ipsuet commodo. Cras purus odio, vestibulum in vulputate a Imperdiet interdum donec eget metus auguen unc vel lorem.</p>
                                        <!-- Button-->

                                    </div>
                                </div>
                            </div>
                            <!-- /box_icon -->
                        </div>
                        <!-- /col-xs-12 ends -->
                        <!-- Feature Box 3  -->
                        <div class="col-xs-12">
                            <div class="box_icon">
                                <div class="icon">
                                    <!-- icon -->
                                    <div class="image">
                                        <img src="{{asset('img/service3.jpg')}}" class="img-responsive" alt="">
                                    </div>
                                    <div class="info">
                                        <h4>Accessories</h4>
                                        <p>Nulla vel metus scelerisque ante sollicitudinlorem ipsuet commodo. Cras purus odio, vestibulum in vulputate a Imperdiet interdum donec eget metus auguen unc vel lorem.</p>
                                        <!-- Button-->

                                    </div>
                                </div>
                            </div>
                            <!-- /box_icon -->
                        </div>
                        <!-- /col-xs-12 ends -->
                        <!-- Feature Box 4  -->

                        <!-- /col-xs-12 ends -->
                        <!-- Feature Box 5  -->

                        <!-- /box_icon -->
                    </div>
                    <!-- /col-xs-12 ends -->
                </div>
                <!-- /owl-services -->
            </div>
            <!-- /col-md-9 -->
        </div>
        <!-- /row -->
        </div>
        <!-- /container-fluid-->
    </section>
    <!-- Section About-index -->
    <section id="about-index" class="bg-lightcolor1">
        <div class="container">
            <div class="section-heading text-center">
                <h2>Tentang Kami</h2>
            </div>
            <!-- row -->
            <div class="row">
                <div class="col-md-6 col-lg-7 text-center">
                    <h3>Menjadi Teman Baik Anda</h3>
                    <p>
                        Anda telah memutuskan untuk memberikan kucing dari tempat penampungan rumah selamanya-hal yang fantastis untuk dilakukan! Ada begitu banyak hewan peliharaan di Selandia Baru yang telah ditelantarkan, disalahgunakan, atau hanya memiliki pemilik yang tidak dapat lagi merawatnya. Semua hewan ini mencari kesempatan kedua di rumah yang penuh kasih.
                    </p>
                    <!-- Accordion -->
                    <div class="panel-group" id="accordion">
                        <!-- Question 1 -->
                        <div class="panel">
                            <div class="panel-heading">
                                <h5 class="panel-title">
                                    <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion" href="#collapseOne">Social Responsability</a>
                                </h5>
                            </div>
                            <div id="collapseOne" class="panel-collapse collapse in">
                                <div class="panel-body">
                                    <p>Baik Anda ingin mengadopsi anak kucing atau kucing dewasa dengan kepribadian yang sepenuhnya terbentuk dan unik, ada banyak hal yang perlu dipertimbangkan sebelum membawa pulang anggota keluarga baru Anda. Cari tahu cara mengadopsi kucing dan apa yang Anda perlukan untuk kedatangan baru Anda dengan panduan ini.
                                    </p>
                                </div>
                            </div>
                        </div>
                        <!-- Question 2 -->
                        <div class="panel">
                            <div class="panel-heading">
                                <h5 class="panel-title">
                                    <a class="accordion-toggle collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapseTwo">Mission Statement</a>
                                </h5>
                            </div>
                            <div id="collapseTwo" class="panel-collapse collapse">
                                <div class="panel-body">
                                    <p>Saat mempertimbangkan cara mengadopsi kucing, hal pertama yang perlu Anda lakukan adalah menemukan tempat penampungan atau badan amal yang sesuai dengan reputasi yang mapan. Anda dapat dengan mudah menemukan banyak dari ini dengan mencari tempat penampungan lokal Anda secara online.
                                    </p>
                                </div>
                            </div>
                        </div>
                        <!-- Question 3 -->
                        <div class="panel">
                            <div class="panel-heading">
                                <h5 class="panel-title">
                                    <a class="accordion-toggle collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapseThree">Value Added Services</a>
                                </h5>
                            </div>
                            <div id="collapseThree" class="panel-collapse collapse">
                                <div class="panel-body">
                                    <p>Salah satu hal terpenting saat mengadopsi kucing adalah memastikan bahwa mereka memiliki kepribadian yang tepat untuk menyesuaikan diri dengan keluarga Anda saat ini. Shelter akan dapat memberi Anda riwayat terperinci tentang latar belakang penghuninya, serta memberi saran tentang kucing mana yang paling cocok untuk keluarga Anda.
                                    </p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- /.accordion -->
                </div>
                <!-- /col-md-7-->

                <div class="col-md-6 col-lg-5">
                    <img src="{{asset('img/about-index.png')}}" class="img-responsive" alt="">
                </div>
                <!-- /col-md-5-->
            </div>
            <!-- /row -->
            <div class="row margin1 text-center">
                <!-- item 1 -->

                <!-- /col-md-3 -->
                <!-- item 2 -->

                <!-- /col-md-3 -->
                <!-- item 3 -->

                <!-- /col-md-3 -->
                <!-- item 1 -->

                <!-- /col-md-3 -->
            </div>
            <!-- /row -->
        </div>
        <!-- /container -->
    </section>
    <!-- /section ends -->
    <!-- /container-fluid -->
    <!-- Section Reviews -->
    <section id="reviews">
        <div class="container">
            <div class="section-heading text-center">
                <h2>Clients Reviews</h2>
            </div>
            <!-- Parallax object -->
            <div class="parallax-object2 hidden-sm hidden-xs hidden-md" data-100-start="transform:rotate(-0deg); left:20%;" data-top-bottom="transform:rotate(-370deg);">
                <!-- Image -->
                <img src="{{asset('img/illustrations/toy.png')}}" alt="">
            </div>
            <!-- /col-md-3 -->
            <div class="col-md-9 col-md-offset-3">
                <!-- Reviews -->
                <div id="owl-reviews" class="owl-carousel margin1">
                    <!-- review 1 -->
                    <div class="review">
                        <div class="col-xs-12">
                            <!-- caption -->
                            <div class="review-caption">
                                <h5>Sue Shei</h5>
                                <div class="small-heading">
                                    Dog Lover
                                </div>
                                <blockquote>
                                    Patatemp dolupta orem retibusam qui commolu
                                    Fusce mollis imperdiet interdum donec eget metus auguen unc vel mauris ultricies,
                                    vest ibulum orci eget, viverra.
                                </blockquote>
                            </div>
                            <!-- Profile image -->
                            <div class="review-profile-image">
                                <img src="{{asset('img/review1.jpg')}}" alt="" />
                            </div>
                            <!--/review profile image -->
                        </div>
                        <!-- /col-xs-12 ends -->
                    </div>
                    <!-- /review-->
                    <!-- review 2 -->
                    <div class="review">
                        <div class="col-xs-12">
                            <!-- caption -->
                            <div class="review-caption">
                                <h5>Jonas Smith</h5>
                                <div class="small-heading">
                                    Cat Specialist
                                </div>
                                <blockquote>
                                    Patatemp dolupta orem retibusam qui commolu
                                    Fusce mollis imperdiet interdum donec eget metus auguen unc vel mauris ultricies,
                                    vest ibulum orci eget, viverra.
                                </blockquote>
                            </div>
                            <!-- Profile image -->
                            <div class="review-profile-image">
                                <img src="{{asset('img/review2.jpg')}}" alt="" />
                            </div>
                            <!--/review profile image -->
                        </div>
                        <!-- /col-xs-12 ends -->
                    </div>
                    <!-- /review-->
                    <!-- review 3 -->
                    <div class="review">
                        <div class="col-xs-12">
                            <!-- caption -->
                            <div class="review-caption">
                                <h5>Maria Silva</h5>
                                <div class="small-heading">
                                    Exotic Birds Vet
                                </div>
                                <blockquote>
                                    Patatemp dolupta orem retibusam qui commolu
                                    Fusce mollis imperdiet interdum donec eget metus auguen unc vel mauris ultricies,
                                    vest ibulum orci eget, viverra.
                                </blockquote>
                            </div>
                            <!-- Profile image -->
                            <div class="review-profile-image">
                                <img src="{{asset('img/review3.jpg')}}" alt="" />
                            </div>
                            <!--/review profile image -->
                        </div>
                        <!-- /col-xs-12 ends -->
                    </div>
                    <!-- /review-->
                    <!-- review 4 -->
                    <div class="review">
                        <div class="col-xs-12">
                            <!-- caption -->
                            <div class="review-caption">
                                <h5>Lou Lou</h5>
                                <div class="small-heading">
                                    Veterinarian
                                </div>
                                <blockquote>
                                    Patatemp dolupta orem retibusam qui commolu
                                    Fusce mollis imperdiet interdum donec eget metus auguen unc vel mauris ultricies,
                                    vest ibulum orci eget, viverra.
                                </blockquote>
                            </div>
                            <!-- Profile image -->
                            <div class="review-profile-image">
                                <img src="{{asset('img/review4.jpg')}}" alt="" />
                            </div>
                            <!--/review profile image -->
                        </div>
                        <!-- /col-xs-12 ends -->
                    </div>
                    <!-- /review-->
                    <!-- review 4 -->
                    <div class="review">
                        <div class="col-xs-12">
                            <!-- caption -->
                            <div class="review-caption">
                                <h5>James Doe</h5>
                                <div class="small-heading">
                                    Pet Lover
                                </div>
                                <blockquote>
                                    Patatemp dolupta orem retibusam qui commolu
                                    Fusce mollis imperdiet interdum donec eget metus auguen unc vel mauris ultricies,
                                    vest ibulum orci eget, viverra.
                                </blockquote>
                            </div>
                            <!-- Profile image -->
                            <div class="review-profile-image">
                                <img src="{{asset('img/review5.jpg')}}" alt="" />
                            </div>
                            <!--/review profile image -->
                        </div>
                        <!-- /col-xs-12 ends -->
                    </div>
                    <!-- /review-->
                </div>
                <!-- /owl-carousel -->
            </div>
            <!-- /col-md-10 -->
        </div>
        <!-- /.container -->
    </section>
    <!-- /Section ends -->
    <!-- callout-->

    <!-- /callout -->
    <!-- Section Stats -->

    <!-- Section Ends-->

    <!-- Section Contact  -->
    <section id="about-index" class="bg-lightcolor1">

        <div class="container">
            <div class="section-heading">
                <h2>Hubungi Kami</h2>
            </div>
            <div class="col-lg-7 col-lg-offset-5 col-md-8 col-md-offset-2">
                <!-- contact info -->
                <h4>Informasi </h4>
                <ul class="list-inline">
                    <li><i class="fa fa-envelope"></i><a href="mailto:sahabatmaincoon@gmail.com"> sahabatmaincoon@gmail.com</a></li>
                    <li><img src="{{asset('img/icon/wa.png')}}" alt=""
                      width="20" height="20" >
                      <a href="https://wa.me/628129629276?text=Bagaimana saya mendapatkan informasi ini">62 812-9629-276</a>
                    </li>
                    <li><i class="fa fa-map-marker margin-icon"></i> Depok, Indonesia</li>
                </ul>
                <!-- address info -->
                <p>Kami adalah tim pecinta hewan peliharaan, perawat hewan, spesialis perilaku, dan ahli nutrisi yang siap menjawab semua pertanyaan Anda.
                </p>
                <h4 class="margin1">Tulis Pesan</h4>
                <!-- Form Starts -->
                <div id="contact_form">
                    <div class="form-group">
                        <label>Nama<span class="required">*</span></label>
                        <input type="text" name="name" class="form-control input-field" required="">
                        <label>Email<span class="required">*</span></label>
                        <input type="email" name="email" class="form-control input-field" required="">
                        <label>Subjek</label>
                        <input type="text" name="subject" class="form-control input-field" required="">
                        <label>Pesan<span class="required">*</span></label>
                        <textarea name="message" id="message" class="textarea-field form-control" rows="3" required=""></textarea>
                    </div>
                    <button type="submit" id="submit_btn" value="Submit" class="btn center-block">Kirim Pesan</button>
                </div>
                <!-- Contact results -->
                <div id="contact_results"></div>
            </div>
            <!-- Contact -->
            <!-- /col-lg-5-->
        </div>
        <!-- /container -->
    </section>
    <!-- /Section ends -->

    <!-- Footer -->
    <footer>
        <!-- Contact info -->
        <div class="container">
            <div class="col-md-4 text-center">
                <!-- Footer logo -->
                <img src="img/Logo-white.png" alt="" class="center-block img-responsive">
            </div>
            <!-- /.col-md-4 -->
            <div class="col-md-4 text-center res-margin">
                <ul class="list-unstyled">
                  <li><img src="{{asset('img/icon/wa.png')}}" alt=""
                    width="20" height="20" >
                    <a href="https://wa.me/628129629276?text=Bagaimana saya mendapatkan informasi ini">62 812-9629-276</a>
                  </li>
                    <li><i class="fa fa-envelope"></i>Email: <a href="mailto:sahabatmaincoon@gmail.com">sahabatmaincoon@gmail.com</a></li>
                    <li><i class="fa fa-map-marker"></i>Sawangan Elok, Parung - Depok</li>
                </ul>
            </div>
            <!-- /.col-md-4 -->
            <div class="col-md-4 text-center res-margin">
                <h5>Follow us</h5>
                <!--Social icons -->
                <div class="social-media">
                    <a href="#" title=""><i class="fa fa-twitter"></i></a>
                    <a href="#" title=""><i class="fa fa-facebook"></i></a>
                    <a href="#" title=""><i class="fa fa-google-plus"></i></a>
                    <a href="#" title=""><i class="fa fa-instagram"></i></a>
                </div>
            </div>
            <!-- /.col-md-4 -->
        </div>
        <!-- /.container -->
        <!-- Credits-->
        <div class="credits col-md-12 text-center">
            Copyright © 2022
            <!-- Go To Top Link -->
            <div class="page-scroll hidden-sm hidden-xs">
                <a href="#page-top" class="back-to-top"><i class="fa fa-angle-up"></i></a>
            </div>
        </div>
        <!-- /credits -->
    </footer>
    <!-- /footer ends -->
    <!-- Core JavaScript Files -->
    <script src="{{ asset('js/jquery.min.js') }}"></script>
    <script src="{{ asset('js/bootstrap.min.js') }}"></script>
    <!-- Main Js -->
    <script src="{{ asset('js/main.js') }}"></script>
    <!-- Contact form -->
    <script src="{{ asset('js/contact.js') }}"></script>
    <!--Other Plugins -->
    <script src="{{ asset('js/plugins.js') }}"></script>
    <!-- Prefix free CSS -->
    <script src="{{ asset('js/prefixfree.js') }}"></script>
    <!--Mail Chimp validator -->
    <script src="{{ asset('js/mc-validate.js') }}"></script>
    <!-- Open street maps-->
    <script src="{{ asset('js/map.js') }}"></script>
</body>

</html>
