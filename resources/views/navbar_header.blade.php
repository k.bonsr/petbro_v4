<nav class="navbar navbar-custom navbar-fixed-top">
    <div class="container">
        <!-- Brand and toggle get grouped for better mobile display -->
        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#navbar-brand-centered">
                <i class="fa fa-bars"></i>
            </button>
            <div class="navbar-brand navbar-brand-centered page-scroll">
                <a href="#page-top">
                    <!-- logo  -->
                    <img src="{{ asset('img/logo.png') }}" class="img-responsive" alt="">
                </a>
            </div>
        </div>
        <!--/navbar-header -->
        <!-- Collect the nav links, forms, and other content for toggling -->
        <div class="collapse navbar-collapse" id="navbar-brand-centered">
            <ul class="nav navbar-nav">
                <li class="active"><a href="/">Home</a></li>
                <li><a href="\about">About</a></li>
                <li><a href="\adoption">Adoption</a></li>
            </ul>
            <ul class="nav navbar-nav navbar-right">
                <li><a href="gallery.html">Gallery</a></li>
                <li><a href="blog.html">Blog</a></li>

                <li><a href="contact.html">Contact</a></li>
            </ul>
        </div>
        <!-- /.navbar-collapse -->
    </div>
    <!-- /.container -->
</nav>
<!-- /navbar ends -->
