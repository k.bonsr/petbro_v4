<!DOCTYPE html>
<html>

@extends('head')

<body id="page-top">
    @extends('preloader')
    @extends('navbar_header')
    @include('index_partial/slider_container')

    <!-- SVG Curve Up -->
    <svg id="curveUp" xmlns="http://www.w3.org/2000/svg" version="1.1" width="100%" height="100" viewBox="0 0 100 100" preserveAspectRatio="none" fill="#fff">
        <path d="M0 100 C 20 0 50 0 100 100 Z" />
    </svg>

    @include('index_partial/service_container')
    @include('index_partial/about_container')




    <!-- /container-fluid -->

    @include('index_partial/review_container')


    <!-- callout-->

    <!-- /callout -->
    <!-- Section Stats -->

    <!-- Section Ends-->

    <!-- Section Contact  -->
    <section id="about-index" class="bg-lightcolor1">

        <div class="container">
            <div class="section-heading">
                <h2>Hubungi Kami</h2>
            </div>
            <div class="col-lg-7 col-lg-offset-5 col-md-8 col-md-offset-2">
                <!-- contact info -->
                <h4>Informasi </h4>
                <ul class="list-inline">
                    <li><i class="fa fa-envelope"></i><a href="mailto:sahabatmaincoon@gmail.com"> sahabatmaincoon@gmail.com</a></li>
                    <li><img src="{{asset('img/icon/wa.png')}}" alt=""
                      width="20" height="20" >
                      <a href="https://wa.me/628129629276?text=Bagaimana saya mendapatkan informasi ini">62 812-9629-276</a>
                    </li>
                    <li><i class="fa fa-map-marker margin-icon"></i> Depok, Indonesia</li>
                </ul>
                <!-- address info -->
                <p>Kami adalah tim pecinta hewan peliharaan, perawat hewan, spesialis perilaku, dan ahli nutrisi yang siap menjawab semua pertanyaan Anda.
                </p>
                <h4 class="margin1">Tulis Pesan</h4>
                <!-- Form Starts -->
                <div id="contact_form">
                    <div class="form-group">
                        <label>Nama<span class="required">*</span></label>
                        <input type="text" name="name" class="form-control input-field" required="">
                        <label>Email<span class="required">*</span></label>
                        <input type="email" name="email" class="form-control input-field" required="">
                        <label>Subjek</label>
                        <input type="text" name="subject" class="form-control input-field" required="">
                        <label>Pesan<span class="required">*</span></label>
                        <textarea name="message" id="message" class="textarea-field form-control" rows="3" required=""></textarea>
                    </div>
                    <button type="submit" id="submit_btn" value="Submit" class="btn center-block">Kirim Pesan</button>
                </div>
                <!-- Contact results -->
                <div id="contact_results"></div>
            </div>
            <!-- Contact -->
            <!-- /col-lg-5-->
        </div>
        <!-- /container -->
    </section>
    <!-- /Section ends -->

    @extends('footer')

</body>

</html>
