<!DOCTYPE html>
<html>

@extends('head')

<body id="page-top">
    @extends('preloader')
    @extends('navbar_header')

    <!-- Section Adopt -->
    <section id="adoption" class="pages">
        <div class="jumbotron" data-stellar-background-ratio="0.5">
            <!-- Heading -->
            <div class="jumbo-heading" data-stellar-background-ratio="1.2">
                <h1>Adoption</h1>
            </div>
        </div>
        <!-- container-->
        <div class="container">
            <!-- row -->
            <div class="row">
                <div class="col-lg-6 col-md-6">
                    <!-- image -->
                    <img src="img/adopt.jpg" alt="" class="center-block img-responsive img-rounded">
                </div>
                <!-- /col-lg-6 -->
                <div class="col-lg-6 col-md-6 res-margin">
                    <h3>Adopt a Friend</h3>
                    <p>Elit uasi quidem minus id omnis a nibh fusce mollis imperdie tlorem ipuset phas ellus ac sodales Lorem ipsum dolor Phas ellus
                        elit uasi quidem minus id omnis a nibh fusce mollis imperdie tlorem ipuset campas fincas
                    </p>
                    <p>Lorem ipsum dolor Phas ellus ac sodales felis tiam non metus,
                        consectetur adipisicing elit uasi quidem minus id omnis a nibh fusce mollis imperdie tlorem ipuset campas fincas
                    </p>
                    <h4>Benefits of adopting</h4>
                    <ul class="custom no-margin">
                        <li>Aliquam erat volut pat. </li>
                        <li>Ibu lum orci eget, viverra elit liquam erat volut pat phas ellus ac.
                        </li>
                        <li>Aliquam erat volut pat phas ellu</li>
                    </ul>
                    <!-- /ul custom-->
                </div>
                <!-- /col-lg-6 -->
            </div>
            <!-- row -->
            <div class="row margin1">
                <!-- Navigation -->
                <div class="text-center col-md-12 margin1">

                </div>
                <!-- Adoption gallery -->
                <div class="col-md-12 margin1">
                    <div id="lightbox">
                        <!-- Image 1 -->
                        <div class="col-lg-4 col-sm-6 col-md-6 dog">
                            <div class="isotope-item">
                                <div class="adoption-thumb">
                                    <img class="img-responsive img-circle" src="img/adopt1.jpg" alt="">
                                    <!-- header-->
                                    <div class="adopt-header">
                                        <h4>Lucy</h4>
                                        <p>Ellus ac sodales Lorem ipsum dolor Phas ellus ac sodales felis tiam non metus. lorem ipsum dolor sit amet, consectetur adipisicing elit uasi quidem minus id omnis a nib.</p>
                                        <a class="btn" href="adopt-single.html">Adopt</a>
                                    </div>
                                </div>
                            </div>
                            <!--/isotope-item -->
                        </div>
                        <!-- col-lg-4 -->
                        <!-- Image 2 -->
                        <div class="col-lg-4 col-sm-6 col-md-6 cat">
                            <div class="isotope-item">
                                <div class="adoption-thumb">
                                    <img class="img-responsive img-circle" src="img/adopt2.jpg" alt="">
                                    <!-- header-->
                                    <div class="adopt-header">
                                        <h4>Gupsy
                                        </h4>
                                        <p>Ellus ac sodales Lorem ipsum dolor Phas ellus ac sodales felis tiam non metus. lorem ipsum dolor sit amet, consectetur adipisicing elit uasi quidem minus id omnis a nib.</p>
                                        <a class="btn" href="adopt-single.html">Adopt</a>
                                    </div>
                                </div>
                            </div>
                            <!--/isotope-item -->
                        </div>
                        <!-- col-lg-4 -->
                        <!-- Image 3 -->
                        <div class="col-lg-4 col-sm-6 col-md-6 cat">
                            <div class="isotope-item">
                                <div class="adoption-thumb">
                                    <img class="img-responsive img-circle" src="img/adopt3.jpg" alt="">
                                    <!-- header-->
                                    <div class="adopt-header">
                                        <h4>Mary</h4>
                                        <p>Ellus ac sodales Lorem ipsum dolor Phas ellus ac sodales felis tiam non metus. lorem ipsum dolor sit amet, consectetur adipisicing elit uasi quidem minus id omnis a nib.</p>
                                        <a class="btn" href="adopt-single.html">Adopt</a>
                                    </div>
                                </div>
                            </div>
                            <!--/isotope-item -->
                        </div>
                        <!-- col-lg-4 -->
                        <!-- Image 4 -->
                        <div class="col-lg-4 col-sm-6 col-md-6 dog">
                            <div class="isotope-item">
                                <div class="adoption-thumb">
                                    <img class="img-responsive img-circle" src="img/adopt4.jpg" alt="">
                                    <!-- header-->
                                    <div class="adopt-header">
                                        <h4>Lucky</h4>
                                        <p>Ellus ac sodales Lorem ipsum dolor Phas ellus ac sodales felis tiam non metus. lorem ipsum dolor sit amet, consectetur adipisicing elit uasi quidem minus id omnis a nib.</p>
                                        <a class="btn" href="adopt-single.html">Adopt</a>
                                    </div>
                                </div>
                            </div>
                            <!--/isotope-item -->
                        </div>
                        <!-- col-lg-3 -->
                        <!-- Image 5 -->
                        <div class="col-lg-4 col-sm-6 col-md-6 dog">
                            <div class="isotope-item">
                                <div class="adoption-thumb">
                                    <img class="img-responsive img-circle" src="img/adopt5.jpg" alt="">
                                    <!-- header-->
                                    <div class="adopt-header">
                                        <h4>Susu</h4>
                                        <p>Ellus ac sodales Lorem ipsum dolor Phas ellus ac sodales felis tiam non metus. lorem ipsum dolor sit amet, consectetur adipisicing elit uasi quidem minus id omnis a nib.</p>
                                        <a class="btn" href="adopt-single.html">Adopt</a>
                                    </div>
                                </div>
                            </div>
                            <!--/isotope-item -->
                        </div>
                        <!-- col-lg-3 -->
                        <!-- Image 6 -->
                        <div class="col-lg-4 col-sm-6 col-md-6 cat">
                            <div class="isotope-item">
                                <div class="adoption-thumb">
                                    <img class="img-responsive img-circle" src="img/adopt6.jpg" alt="">
                                    <!-- header-->
                                    <div class="adopt-header">
                                        <h4>Ava</h4>
                                        <p>Ellus ac sodales Lorem ipsum dolor Phas ellus ac sodales felis tiam non metus. lorem ipsum dolor sit amet, consectetur adipisicing elit uasi quidem minus id omnis a nib.</p>
                                        <a class="btn" href="adopt-single.html">Adopt</a>
                                    </div>
                                </div>
                            </div>
                            <!--/isotope-item -->
                        </div>
                        <!-- col-lg-4 -->
                        <!-- Image 7 -->
                        <div class="col-lg-4 col-sm-6 col-md-6 cat">
                            <div class="isotope-item">
                                <div class="adoption-thumb">
                                    <img class="img-responsive img-circle" src="img/adopt7.jpg" alt="">
                                    <!-- header-->
                                    <div class="adopt-header">
                                        <h4>Lily</h4>
                                        <p>Ellus ac sodales Lorem ipsum dolor Phas ellus ac sodales felis tiam non metus. lorem ipsum dolor sit amet, consectetur adipisicing elit uasi quidem minus id omnis a nib.</p>
                                        <a class="btn" href="adopt-single.html">Adopt</a>
                                    </div>
                                </div>
                            </div>
                            <!--/isotope-item -->
                        </div>
                        <!-- col-lg-4 -->
                        <!-- Image 8 -->
                        <div class="col-lg-4 col-sm-6 col-md-6 dog">
                            <div class="isotope-item">
                                <div class="adoption-thumb">
                                    <img class="img-responsive img-circle" src="img/adopt8.jpg" alt="">
                                    <!-- header-->
                                    <div class="adopt-header">
                                        <h4>Douggie</h4>
                                        <p>Ellus ac sodales Lorem ipsum dolor Phas ellus ac sodales felis tiam non metus. lorem ipsum dolor sit amet, consectetur adipisicing elit uasi quidem minus id omnis a nib.</p>
                                        <a class="btn" href="adopt-single.html">Adopt</a>
                                    </div>
                                </div>
                            </div>
                            <!--/isotope-item -->
                        </div>
                        <!-- col-lg-4 -->
                    </div>
                    <!-- /lightbox-->
                </div>
                <!-- /col-lg-12 -->
            </div>
            <!-- /row-->
        </div>
        <!-- /container-->
    </section>
    <!-- /Section ends -->

    @extends('footer')

</body>

</html>
