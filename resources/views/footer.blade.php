<!-- Footer -->
<footer>
    <!-- Contact info -->
    <div class="container">
        <div class="col-md-4 text-center">
            <!-- Footer logo -->
            <img src="img/Logo-white.png" alt="" class="center-block img-responsive">
        </div>
        <!-- /.col-md-4 -->
        <div class="col-md-4 text-center res-margin">
            <ul class="list-unstyled">
              <li><img src="{{asset('img/icon/wa.png')}}" alt=""
                width="20" height="20" >
                <a href="https://wa.me/628129629276?text=Bagaimana saya mendapatkan informasi ini">62 812-9629-276</a>
              </li>
                <li><i class="fa fa-envelope"></i>Email: <a href="mailto:sahabatmaincoon@gmail.com">sahabatmaincoon@gmail.com</a></li>
                <li><i class="fa fa-map-marker"></i>Sawangan Elok, Parung - Depok</li>
            </ul>
        </div>
        <!-- /.col-md-4 -->
        <div class="col-md-4 text-center res-margin">
            <h5>Follow us</h5>
            <!--Social icons -->
            <div class="social-media">
                <a href="#" title=""><i class="fa fa-twitter"></i></a>
                <a href="#" title=""><i class="fa fa-facebook"></i></a>
                <a href="#" title=""><i class="fa fa-google-plus"></i></a>
                <a href="#" title=""><i class="fa fa-instagram"></i></a>
            </div>
        </div>
        <!-- /.col-md-4 -->
    </div>
    <!-- /.container -->
    <!-- Credits-->
    <div class="credits col-md-12 text-center">
        Copyright © 2022
        <!-- Go To Top Link -->
        <div class="page-scroll hidden-sm hidden-xs">
            <a href="#page-top" class="back-to-top"><i class="fa fa-angle-up"></i></a>
        </div>
    </div>
    <!-- /credits -->
</footer>
<!-- /footer ends -->
<!-- Core JavaScript Files -->
<script src="{{ asset('js/jquery.min.js') }}"></script>
<script src="{{ asset('js/bootstrap.min.js') }}"></script>
<!-- Main Js -->
<script src="{{ asset('js/main.js') }}"></script>
<!-- Contact form -->
<script src="{{ asset('js/contact.js') }}"></script>
<!--Other Plugins -->
<script src="{{ asset('js/plugins.js') }}"></script>
<!-- Prefix free CSS -->
<script src="{{ asset('js/prefixfree.js') }}"></script>
<!--Mail Chimp validator -->
<script src="{{ asset('js/mc-validate.js') }}"></script>
<!-- Open street maps-->
<script src="{{ asset('js/map.js') }}"></script>
