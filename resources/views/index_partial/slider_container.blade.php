<!-- Full Page Image Background Slider -->
<div class="slider-container">
    <!-- Controls -->
    <div class="slider-control left inactive"></div>
    <div class="slider-control right"></div>
    <ul class="slider-pagi"></ul>
    <!--Slider -->
    <div class="slider">
      @for($i=0; $i <= 2; $i++)
        <?php
          switch ($i) {
            case 0:
              $text_heading = ['Sahabat Mainecoon','Hallo Sahabat mainecoon','gallery.html','our gallery'];
              break;
            case 1:
              $text_heading = ['We Love Pets!','We offer all the best quality products for your best friend.',
                'blog.html','our Blog'];
              break;
            case 2:
              $text_heading = ['Amazing Services','We offer all the best quality products for your best friend.',
                'about.html','About Us'];
              break;
            default:
              $text_heading = ['','','',''];
              break;
          }
        ?>
        <div class="slide slide-{{$i}} active" style="background-image:url('img/slide{{$i}}.jpg')">
            <div class="slide__bg"></div>
            <div class="slide__content">
                <div class="slide__overlay">
                </div>
                <!-- slide text-->
                <div class="slide__text">
                    <h1 class="slide__text-heading">{{$text_heading[0]}}</h1>
                    <div class="hidden-mobile">
                        <p class="lead">{{$text_heading[1]}}</p>
                        <a href="{{$text_heading[2]}}" class="btn btn-default">{{$text_heading[3]}}</a>
                    </div>
                </div>
            </div>
        </div>
      @endfor
    </div>
    <!--/Slider-->
</div>
<!--/ Slider ends -->
