<!-- Section Services-index -->
<section id="services-index">
    <!-- container -->
    <div class="container">
        <div class="section-heading">
            <h2>Pelayanan Kami</h2>
        </div>
        <!-- /section-heading-->
        <div class="col-md-10 col-md-offset-1 text-center">
            <p>Ada begitu banyak manfaat mengadopsi kucing, yaitu memberi hewan peliharaan kesempatan kedua dalam kehidupan keluarga yang terpenuhi! Jika Anda bertanya-tanya bagaimana cara mengadopsi kucing dan apa yang diharapkan selama proses tersebut, Sahabat Mainecoon siap membantu.</p>
        </div>
        <!-- /col-md-10-->
    </div>
    <!-- /container-->
    <div class="container-fluid bg-pattern margin1" data-bottom-top="background-position: 0px 70%,99% 70%;" data-top-bottom="background-position: 0px -50%,99% -50%;">
        <div class="row">
            <div class="col-md-10 col-md-offset-1">
                <!-- Services -->
                <div id="owl-services" class="owl-carousel">
                    <!-- Feature Box 1  -->
                    @for($i=1; $i <= 3; $i++)
                      <?php
                        switch ($i) {
                          case 1:
                            $text_service = ['Grooming',
                              'Nulla vel metus scelerisque ante sollicitudinlorem ipsuet commodo. Cras purus odio, vestibulum in vulputate a Imperdiet interdum donec eget metus auguen unc vel lorem.'];
                            break;
                          case 2:
                            $text_service = ['Breeding',
                              'Nulla vel metus scelerisque ante sollicitudinlorem ipsuet commodo. Cras purus odio, vestibulum in vulputate a Imperdiet interdum donec eget metus auguen unc vel lorem.'];
                            break;
                          case 3:
                            $text_service = ['Accessories',
                              'Nulla vel metus scelerisque ante sollicitudinlorem ipsuet commodo. Cras purus odio, vestibulum in vulputate a Imperdiet interdum donec eget metus auguen unc vel lorem.'];
                            break;
                          default:
                            $text_service = ['',''];
                            break;
                        }
                      ?>
                      <div class="col-xs-12">
                          <div class="box_icon">
                              <div class="icon">
                                  <!-- icon -->
                                  <div class="image">
                                      <img src="{{ asset('img/service'.$i.'.jpg')}}" class="img-responsive" alt="">
                                  </div>
                                  <div class="info">
                                      <h4>{{$text_service[0]}}</h4>
                                      <p>{{$text_service[1]}}</p>
                                      <!-- Button-->

                                  </div>
                              </div>
                          </div>
                          <!-- /box_icon -->
                      </div>
                      <!-- /col-xs-12 ends -->
                    @endfor

                </div>
                <!-- /col-xs-12 ends -->
            </div>
            <!-- /owl-services -->
        </div>
        <!-- /col-md-9 -->
    </div>
    <!-- /row -->
    </div>
    <!-- /container-fluid-->
</section>
