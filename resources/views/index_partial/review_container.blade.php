<!-- Section Reviews -->
<section id="reviews">
    <div class="container">
        <div class="section-heading text-center">
            <h2>Clients Reviews</h2>
        </div>
        <!-- Parallax object -->
        <div class="parallax-object2 hidden-sm hidden-xs hidden-md" data-100-start="transform:rotate(-0deg); left:20%;" data-top-bottom="transform:rotate(-370deg);">
            <!-- Image -->
            <img src="{{asset('img/illustrations/toy.png')}}" alt="">
        </div>
        <!-- /col-md-3 -->
        <div class="col-md-9 col-md-offset-3">
            <!-- Reviews -->
            <div id="owl-reviews" class="owl-carousel margin1">
                @for($i=1; $i <= 5; $i++)
                  <?php
                    switch ($i) {
                      case 1:
                        $text_review = ['Sue Shei', 'Dog Lover',
                          'Patatemp dolupta orem retibusam qui commolu Fusce mollis imperdiet interdum donec eget metus auguen unc vel mauris ultricies, vest ibulum orci eget, viverra.'];
                        break;
                      case 2:
                        $text_review = ['Jonas Smith', 'Cat Specialist',
                          'Patatemp dolupta orem retibusam qui commolu Fusce mollis imperdiet interdum donec eget metus auguen unc vel mauris ultricies, vest ibulum orci eget, viverra.'];
                        break;
                      case 3:
                        $text_review = ['Maria Silva', 'Exotic Birds Vet',
                          'Patatemp dolupta orem retibusam qui commolu Fusce mollis imperdiet interdum donec eget metus auguen unc vel mauris ultricies, vest ibulum orci eget, viverra.'];
                        break;
                      case 4:
                        $text_review = ['Lou Lou', 'Veterinarian',
                          'Patatemp dolupta orem retibusam qui commolu Fusce mollis imperdiet interdum donec eget metus auguen unc vel mauris ultricies, vest ibulum orci eget, viverra.'];
                        break;
                      case 5:
                        $text_review = ['James Doe', 'Pet Lover',
                          'Patatemp dolupta orem retibusam qui commolu Fusce mollis imperdiet interdum donec eget metus auguen unc vel mauris ultricies, vest ibulum orci eget, viverra.'];
                        break;
                      default:
                        $text_about = ['','','',''];
                        break;
                    }
                  ?>
                  <!-- review 1 -->
                  <div class="review">
                      <div class="col-xs-12">
                          <!-- caption -->
                          <div class="review-caption">
                              <h5>{{$text_review[0]}}</h5>
                              <div class="small-heading">
                                  {{$text_review[1]}}
                              </div>
                              <blockquote>
                                  {{$text_review[2]}}
                              </blockquote>
                          </div>
                          <!-- Profile image -->
                          <div class="review-profile-image">
                              <img src="{{asset('img/review'.$i.'.jpg')}}" alt="" />
                          </div>
                          <!--/review profile image -->
                      </div>
                      <!-- /col-xs-12 ends -->
                  </div>
                  <!-- /review-->
                @endfor

            </div>
            <!-- /owl-carousel -->
        </div>
        <!-- /col-md-10 -->
    </div>
    <!-- /.container -->
</section>
<!-- /Section ends -->
