<!-- Section About-index -->
<section id="about-index" class="bg-lightcolor1">
    <div class="container">
        <div class="section-heading text-center">
            <h2>Tentang Kami</h2>
        </div>
        <!-- row -->
        <div class="row">
            <div class="col-md-6 col-lg-7 text-center">
                <h3>Menjadi Teman Baik Anda</h3>
                <p>
                    Anda telah memutuskan untuk memberikan kucing dari tempat penampungan rumah selamanya-hal yang fantastis untuk dilakukan! Ada begitu banyak hewan peliharaan di Selandia Baru yang telah ditelantarkan, disalahgunakan, atau hanya memiliki pemilik yang tidak dapat lagi merawatnya. Semua hewan ini mencari kesempatan kedua di rumah yang penuh kasih.
                </p>
                <!-- Accordion -->
                <div class="panel-group" id="accordion">
                    @for($i=1; $i <= 3; $i++)
                      <?php
                        $collapse_active = '';
                        if ($i == 1) {
                          $collapse_active = 'in';
                        }

                        switch ($i) {
                          case 1:
                            $text_about = ['Social Responsability',
                              'Baik Anda ingin mengadopsi anak kucing atau kucing dewasa dengan kepribadian yang sepenuhnya terbentuk dan unik, ada banyak hal yang perlu dipertimbangkan sebelum membawa pulang anggota keluarga baru Anda. Cari tahu cara mengadopsi kucing dan apa yang Anda perlukan untuk kedatangan baru Anda dengan panduan ini.'];
                            break;
                          case 2:
                            $text_about = ['Mission Statement',
                              'Saat mempertimbangkan cara mengadopsi kucing, hal pertama yang perlu Anda lakukan adalah menemukan tempat penampungan atau badan amal yang sesuai dengan reputasi yang mapan. Anda dapat dengan mudah menemukan banyak dari ini dengan mencari tempat penampungan lokal Anda secara online.'];
                            break;
                          case 3:
                            $text_about = ['Value Added Services',
                              'Salah satu hal terpenting saat mengadopsi kucing adalah memastikan bahwa mereka memiliki kepribadian yang tepat untuk menyesuaikan diri dengan keluarga Anda saat ini. Shelter akan dapat memberi Anda riwayat terperinci tentang latar belakang penghuninya, serta memberi saran tentang kucing mana yang paling cocok untuk keluarga Anda.'];
                            break;
                          default:
                            $text_about = ['','','',''];
                            break;
                        }
                      ?>
                    <!-- Question 1 -->
                      <div class="panel">
                          <div class="panel-heading">
                              <h5 class="panel-title">
                                  <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion" href="#collapse{{$i}}">{{$text_about[0]}}</a>
                              </h5>
                          </div>
                          <div id="collapse{{$i}}" class="panel-collapse collapse {{$collapse_active}}">
                              <div class="panel-body">
                                  <p>{{$text_about[1]}}</p>
                              </div>
                          </div>
                      </div>
                    @endfor
                </div>
                <!-- /.accordion -->
            </div>
            <!-- /col-md-7-->

            <div class="col-md-6 col-lg-5">
                <img src="{{asset('img/about-index.png')}}" class="img-responsive" alt="">
            </div>
            <!-- /col-md-5-->
        </div>
        <!-- /row -->
        <div class="row margin1 text-center">
            <!-- item 1 -->

            <!-- /col-md-3 -->
            <!-- item 2 -->

            <!-- /col-md-3 -->
            <!-- item 3 -->

            <!-- /col-md-3 -->
            <!-- item 1 -->

            <!-- /col-md-3 -->
        </div>
        <!-- /row -->
    </div>
    <!-- /container -->
</section>
<!-- /section ends -->
